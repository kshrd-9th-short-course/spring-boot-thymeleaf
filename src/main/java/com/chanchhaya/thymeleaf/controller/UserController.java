package com.chanchhaya.thymeleaf.controller;

import com.chanchhaya.thymeleaf.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class UserController {

    @GetMapping("/user/add")
    public String viewUserAdd(ModelMap modelMap, User user) {

        modelMap.addAttribute("user", user);

        return "user/add-user";
    }

    @PostMapping("/user/add")
    public String addUser(@Valid User user, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("User data error");
            System.out.println(result.getFieldError("email"));
            return "user/add-user";
        }

        user.setUuid(UUID.randomUUID().toString());

        System.out.println("Add user = " + user);
        return "user/list-user";
    }

    @GetMapping("/user/detail")
    public String viewUserDetailByParam(@RequestParam String username,
                                        @RequestParam int id,
                                        ModelMap modelMap) {

        modelMap.addAttribute("username", username);
        modelMap.addAttribute("id", id);
        modelMap.addAttribute("imageLink", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcST5F7_lviwof-a8lFlKVx-WOv9xmU4XPoP1Q&usqp=CAU");
        modelMap.addAttribute("email", "<h3>Dear everyone</h3>");
        modelMap.addAttribute("image", "<img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcST5F7_lviwof-a8lFlKVx-WOv9xmU4XPoP1Q&usqp=CAU'>");

        return "user/detail-user";

    }

    @GetMapping("/user/{username}/{id}")
    public String viewUserDetail(@PathVariable String username,
                                 @PathVariable int id,
                                 ModelMap modelMap) {

        modelMap.addAttribute("username", username);
        modelMap.addAttribute("id", id);

        return "user/detail-user";
    }

    @GetMapping("/user")
    public String viewUser(ModelMap modelMap) {

        System.out.println("List user");

        return "user/list-user";
    }

}
